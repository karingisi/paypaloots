<?php
namespace Craft;

class PaypalootsRecord extends BaseRecord
{
    public function getTableName()
    {
        return 'paypaloots_records';
    }

    public function defineAttributes()
    {
        return array(
            
            'type' => array(AttributeType::String, 'required' => true),  
            'amount' => array(AttributeType::String, 'required' => true),
            'currency' => array(AttributeType::String, 'required' => true),
            'funding_source' => array(AttributeType::String,'required' => true),
            'payment_resource' => array(AttributeType::String, 'required' => true),
            'transaction_id' => array(AttributeType::String, 'required' => true),
            'status' => array(AttributeType::String, 'required' => true),  //status(Approved)
        );
    }    

    /**
     * Create a new instance of the current class. This allows us to
     * properly unit test our service layer.
     *
     * @return BaseRecord
     */
    public function create()
    {
        $class = get_class($this);
        $record = new $class();

        return $record;
    }     
}