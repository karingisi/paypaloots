<?php  
namespace Craft;

use Twig_Extension;  
use Twig_Filter_Method;

class ArrayCastTwigExtension extends \Twig_Extension  
{
    
    public function getName()
    {
        return 'ArrayCast';
    }

    public function getFilters()
    {
        return array(
            'arrayCast' => new Twig_Filter_Method($this, 'arrayCast'),
        );
    }

    public function arrayCast($stdClassObject) 
    {
        $response = array();

        foreach ($stdClassObject as $key => $value) {

            $response[$key] = $value;
        }
        return $response;
    }
}