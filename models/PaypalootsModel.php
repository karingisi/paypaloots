<?php
namespace Craft;

class PaypalootsModel extends BaseModel
{
	protected function defineAttributes()
	{
		return array(

			'id'    => AttributeType::Number,
			'dateCreated'=>AttributeType::DateTime,  //add this if you want to query record using dateCreated
			'type' => array(AttributeType::String, 'required' => true),
            'amount' => array(AttributeType::String, 'required' => true),
            'currency' => array(AttributeType::String, 'required' => true),
  
            'funding_source' => array(AttributeType::String,'required' => true),
            'payment_resource' => array(AttributeType::String, 'required' => true),
            'transaction_id' => array(AttributeType::String, 'required' => true),
            'status' => array(AttributeType::String, 'required' => true),  //status(Approved)
		);
	}
}