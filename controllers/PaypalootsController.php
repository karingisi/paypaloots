<?php
namespace Craft;

/**
 * Paypaloots controller
 */
class PaypalootsController extends BaseController
{
	/**
	 * @var Allows anonymous access to this controller's actions.
	 * @access protected
	 */
	protected $allowAnonymous = true;
	//protected $allowAnonymous = array('actionMember', 'actionPayment', 'actionConfirm', 'actionCancel');

	public function actionPaidInvoices(){

		craft()->paypaloots->paidInvoices();
	}

	//create member listing after saving details
	public function actionListing(){

		craft()->paypaloots->createMemberListing();
	}

	public function actionSendEmail(){

		$toEmail = "silvanus@outofthesquare.com.au";
		$subject = "This is a test email from TSM cronjob";
		$body = "TSM cronjob";
		
		if(craft()->paypaloots->sendEmailTo($toEmail,$subject,$body)){
			echo "email sent";
		}else{
		}
	}

	public function actionMember(){

		$user = new UserModel;
		
		$memberAttributes = craft()->request->getPost();
		
		$user->username = $memberAttributes['email'];
		$user->email = $memberAttributes['email'];


		//$userPhoto = UploadedFile::getInstanceByName('businessImage');

		//var_dump($userPhoto);

		//$userImage =  craft()->request->getPost('businessImage');
		
		if($this->userExist($user)){

            craft()->userSession->setFlash('userExist', "Sorry, account with same email address already exists!");

		}else{

			if($memberAttributes){

				$this->validateForm($memberAttributes); //call validateForm

				if(count(craft()->userSession->getFlashes()) == 0){

					craft()->httpSession->add('member', $memberAttributes);	//add member details to session
					//$businessImage->saveAs($path);   //save uploaded attachment
					//craft()->users->saveUserPhoto($userPhoto->name,$userImage, $user); //add profile picture
					$this->actionPayment();
				
				}else{
					$this->validateForm($memberAttributes); //call validateForm
				}	
			}	
		}
	}

	public function validateForm($memberAttributes){

		$rules = array(

				'email' => array(
									'required'=> 'true',
									'minlength'=> '5',
									'type'=>'email',
									'message' => 'Email is required and must be at least 5 characters long',
									'messageType' => 'Email format is not correct'
								),
				
				/*'tsmNewsletterContactEmail' => array(
									'required'=> 'false',
									'minlength'=> '1',
									'type'=>'email',
									'message' => 'Newsletter Email is required and must be at least 5 characters long',
									'messageType' => 'Newsletter email format is not correct'
								),*/

				'businessName' => array(
									'required'=> 'true',
									'minlength'=> '2',
									'type'=>'string',
									'message' => 'Business Name is required and must be at least 2 characters long'
								),
					
				'phone' => array(
									'required'=> 'true',
									'minlength'=> '6',
									'type'=>'number',
									'message' => 'Phone is required and must be at least 6 characters long',
									'messageType' => 'Phone must be a number only, no spaces please'
								),
				
				'zip' => array(
									'required'=> 'false',
									'minlength'=> '1',
									'type'=>'number',
									'message' => 'Postcode is required and must be at least 6 characters long',
									'messageType' => 'Postcode must be a number only, no spaces please'
								),

				'fax' => array(
									'required'=> 'false',
									'minlength'=> '1',
									'type'=>'number',
									'message' => 'Fax is required and must be at least 10 characters long',
									'messageType' => 'Fax must be a number only, no spaces please'
								),

				'primaryContact' => array(
									'required'=> 'true',
									'minlength'=> '2',
									'type'=>'string',
									'message' => 'Primary Contact is required and must be at least 2 characters long'
								),
				
				'accountsContact' => array(
									'required'=> 'true',
									'minlength'=> '2',
									'type'=>'string',
									'message' => 'Accounts Contact is required and must be at least 2 characters long'
								),
				'body' => array(
									'required'=> 'true',
									'minlength'=> '1',
									'type'=>'string',
									'message' => 'Business description is required'
								),

				'marketingAmount' => array(
									'required'=> 'false',
									'minlength'=> '1',
									'type'=>'number',
									'message' => 'Amount need to be a number',
									'messageType' => 'Amount must be a number only, no spaces or characters please'
								),

		);

		foreach($rules as $key=>$value){
			
			if( $memberAttributes[$key] == "" || strlen($memberAttributes[$key]) < $value['minlength'] ){
				
				if($value['required'] == "true"){
					craft()->userSession->setFlash($key, $value['message']);
				
				}
		
			}else if($value["type"] == "number"){

				if(!is_numeric($memberAttributes[$key])){
					craft()->userSession->setFlash($key, $value['messageType']);
				}
			}else if($value["type"] == "email"){

				if (!filter_var($memberAttributes[$key], FILTER_VALIDATE_EMAIL)) {
					craft()->userSession->setFlash($key, $value['messageType']);
				}
			
			}

			/*else if( $memberAttributes['marketingCampaign'] == "Yes" &&  $memberAttributes['marketingAmount'] == ""){
				
					craft()->userSession->setFlash('marketingAmount', 'Please enter TSM Marketing Contribution Amount');	

			
			}else if( $memberAttributes['regionalChamber'] == "Yes" &&  trim($memberAttributes['regionalChamberName']) == ""){
				
					craft()->userSession->setFlash('regionalChamber', 'Please select regional chamber');	

			}*/

			else{

			}
		}
	}

	public function userExist($user)
	{
		$existingUser = craft()->users->getUserByUsernameOrEmail($user->email);

	    if ($existingUser)
	    {
	    	return true;
	    }
	    return false;
	}

	public function actionIndex()
	{
        // Load index template
    	$this->renderTemplate('paypaloots/index');
	}

	public function actionNew()
	{
        // Load a particular template and with all of the variables you've created
    	$this->renderTemplate('paypaloots/_new');
	}

	public function actionShow()
	{
	  	// Load show template
    	$this->renderTemplate('paypaloots/_show');
	}

	public function actionPayment()
	{
    	craft()->paypaloots->createPaypalPayment();
	}

	// render paypal confirmation, pass confirm variables from payment execute
	public function actionConfirm()
	{
		//$this_variables = craft()->paypaloots->execute();
        $member = craft()->httpSession->get('member'); //get session member data
        //craft()->paypaloots->createMemberListing($member); //should remove this in production
        //add user here
        $expiryDate = date('Y-m-d');

        $user = new UserModel;
    	$user->username = $member['email'];
		$user->email = $member['email'];
		$user->firstName = $member['businessName'];

		$criteria = craft()->elements->getCriteria(ElementType::Category);
    	$criteria->id = $member['businessCategory'];
    	$businessCategory = craft()->elements->findElements($criteria);
    	$businessCategoryArray = array();

    	foreach($businessCategory as $category){
        	array_push($businessCategoryArray,(int)$category->id);
    	}

    	//add businessCategoryDescentant to the businessCategory array
    	$businessCategoryDescentants = $member['businessCategoryDescentant'];
    	foreach($businessCategoryDescentants as $descentant){
    		array_push($businessCategoryArray,(int)$descentant);
    	}

		$user->setContent([
							'businessName' => $member['businessName'],
							'phone' => $member['phone'], 
							'fax'=> $member['fax'], 
							'abn'=> $member['abn'], 
							'websiteAddress'=> $member['websiteAddress'],
							'primaryContact' => $member['primaryContact'],
							'accountsContact' => $member['accountsContact'],
							'regionalChamber' => $member['regionalChamber'],
							'regionalChamberName' => $member['regionalChamberName'],
							'body' => $member['body'],
							'address'=>['street1'=>$member['street1'], 'street2'=>'' ,'city'=>$member['city'], 'state'=>$member['state'],
										'zip'=>$member['zip'], 'country'=>'Australia', 'lat'=>'', 'lng'=>''],
							'businessCategory' => $businessCategoryArray,
							
						  ]);

        if(craft()->users->saveUser($user)){ //save member only on url redirection
        	//assign user to the group 'members' group # 1
        	// We now have a user Id, so assign user to a group
			
			//do matrix field tsm here
			$newsletterContactMatrix = craft()->fields->getFieldByHandle("tsmNewsletterContact");

	        $block = new MatrixBlockModel();
	        $block->fieldId    = $newsletterContactMatrix->id; // Matrix field's ID
	        $block->ownerId    = $user->id; // ID of entry the block should be added to
	        $block->typeId     = 1; // ID of block type for tsm

	        $block->getContent()->setAttributes(array(
	            'contactName' => $member['tsmNewsletterContact'],
	            'contactEmail' => $member['tsmNewsletterContactEmail']
	        ));

			craft()->matrix->saveBlock($block); //save the tsm newsletterContact block
			craft()->userGroups->assignUserToGroups($user->id, $groupId=3); //Members Organisations

			//after saving user create entry into the database
			//craft()->paypaloots->createMemberListing();
        	craft()->users->sendActivationEmail($user);
        	$this->sendEmail(); //send email to TSM contact
        } 
		//$this->renderTemplate('confirm',$this_variables);	
		$this->renderTemplate('confirm');	
	}

	//render paypal cancel
	public function actionCancel()
	{
		$this->renderTemplate('cancel');	
	}

	public function sendEmail(){

		$paypalootsSettings = craft()->plugins->getPlugin('paypaloots')->getSettings();

		$member = craft()->httpSession->get('member'); //get session member data

		$memberDetails = 
					'Business Name : '.$member['businessName'].'<br>'.
					'Email : '.$member['email'].'<br>'.
					'Phone : '.$member['phone'].'<br>'. 
					'Fax : '.$member['fax'].'<br>'.
					'ABN : '.$member['abn'].'<br>'.
					'Website Address : '.$member['websiteAddress'].'<br>'.
					'Primary Contact : '.$member['primaryContact'].'<br>'.
					'Accounts Contact : '.$member['accountsContact'].'<br>'.
					//'Business Category :'.$member['businessCategory'].'<br>'.
					//'Other Category :'.$member['businessCategoryOther'].'<br>'.
					'Newsletter ContactName :'.$member['tsmNewsletterContact'].'<br>'.
					'Newsletter ContactEmail :'.$member['tsmNewsletterContactEmail'].'<br>'.
					'Regional Chamber : '.$member['regionalChamber'].'<br>'.
					'Regional Chamber Name : '.$member['regionalChamberName'].'<br>'.
					'Business Description : '.$member['body'].'<br>'.
					'Street Address : '.$member['street1'].'<br>'.
					'City : '.$member['city'].'<br>'.
					'State: '.$member['state'].'<br>'.
					'Postcode : '.$member['zip'];

		//$toEmails = ArrayHelper::stringToArray($paypalootsSettings->tsmEmail);
		$email = new EmailModel();
		$emailSettings = craft()->email->getSettings();

		$email->fromEmail = $emailSettings['emailAddress'];
		$email->replyTo   = $emailSettings['emailAddress'];
		$email->sender    = $emailSettings['emailAddress'];
		$email->fromName  = "Tourism Snowy Mountains";
		$email->toEmail   = $paypalootsSettings->tsmEmail;
		//$email->cc = $paypalootsSettings->tsmEmailcc;
		$email->subject   = "New Membership Registration";
		$email->body      = "Hi,<br>We have a new member registered via online form.<br><p>Details are as follows:</p>".$memberDetails;

		craft()->email->sendEmail($email);	
	}
	/**
	 * Add Gallery based on posted params.
	 *
	 * @throws Exception
	 */
	public function actionAdd()
	{
		//call the service to add this to db
		$transaction = new PaypalootsModel();

		$transaction->type  = craft()->request->getPost('type');
		$transaction->amount  = craft()->request->getPost('amount');
		$transaction->currency  = craft()->request->getPost('currency');
		$transaction->funding_source  = craft()->request->getPost('funding_source');
		$transaction->payment_resource  = craft()->request->getPost('payment_resource');
		$transaction->transaction_id  = craft()->request->getPost('transaction_id');
		$transaction->status  = craft()->request->getPost('status');

		craft()->paypaloots->add($transaction); //call paypaloots service 
	}

	//create invoice
	public function actionInvoice(){

		$billingEmail = "silvanus.matiku@gmail.com";
		craft()->paypaloots->createInvoice($billingEmail); //call paypaloots service 
	}

	//update invoice
	public function actionUpdateInvoice(){

		$invoiceId = craft()->request->getPost('id');
		$invoiceNumber = craft()->request->getPost('number');

		if(craft()->paypaloots->updateInvoice($invoiceId, $invoiceNumber)){
			$this->redirect("paypaloots/index");
		}
	}

	//call list invoice
	public function actionListInvoice(){
		craft()->paypaloots->listInvoice(); //call paypaloots service 
	}

	public function actionEditInvoice()
	{
	  	// render the template, pass model on render template
    	$this->renderTemplate('paypaloots/_edit');
	}
	/**
	 * Returns a 'success' response.
	 *
	 * @return void
	 */
	protected function returnSuccess()
	{
		if (craft()->request->isAjaxRequest())
		{
			$this->returnJson(array('success' => true));
		}
		else
		{
			// Deprecated. Use 'redirect' instead.
			$successRedirectUrl = craft()->request->getPost('successRedirectUrl');

			if ($successRedirectUrl)
			{
				$_POST['redirect'] = $successRedirectUrl;
			}

			craft()->userSession->setNotice('Payment completed.');
			$this->redirectToPostedUrl();
		}
	}
}