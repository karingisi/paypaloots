<?php

namespace Craft;

/**
 * Paypaloots Variable provides access to database objects from templates ie. from Admin CP
 */
class PaypalootsVariable
{
    /**
     * Get all available Messages
     *
     * @return array
     */
    public function getAllTransactions()
    {
        return craft()->paypaloots->getAllTransactions();
    }

    //get transaction by Id
    public function getTransactionById($id)
    {
        return craft()->paypaloots->getTransactionById($id);
    }

    //get invoice list
    public function getAllInvoices()
    {
        return craft()->paypaloots->listInvoice();
    }

    public function getInvoiceById($id){
        return craft()->paypaloots->getInvoiceById($id);
    } 

    //get paid invoices
    public function getPaidInvoices(){
        return craft()->paypaloots->paidInvoices();
    } 
}