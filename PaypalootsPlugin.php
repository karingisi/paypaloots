<?php
namespace Craft;

class PaypalootsPlugin extends BasePlugin
{
	function getName()
	{
		return 'Paypal';
	}

	function getVersion()
	{
		return '1.0';
	}

	function getDeveloper()
	{
		return 'Out Of The Square Media - OOTS';
	}

	function getDeveloperUrl()
	{
		return 'http://outofthesquare.com.au';
	}

	protected function defineSettings()
	{
		return array(
			'mode' => array(AttributeType::String, 'required' => true),
			'endPoint' => array(AttributeType::String, 'required' => true),
			'clientId' => array(AttributeType::String, 'required' => true),
			'secret'   => array(AttributeType::String, 'required' => true),
			'membershipDescription'   => array(AttributeType::String, 'required' => true),
			'membershipFee'   => array(AttributeType::String, 'required' => true),
			'chamberFeeDiscount'   => array(AttributeType::String, 'required' => true),
			'tsmEmail'   => array(AttributeType::String, 'required' => true),
			'tsmEmailcc'   => array(AttributeType::String, 'required' => true),
			'paypalEmail'   => array(AttributeType::String, 'required' => true),
			'streetAddress'   => array(AttributeType::String, 'required' => true),
			'city'   => array(AttributeType::String, 'required' => true),
			'postcode'   => array(AttributeType::String, 'required' => true),
			'phoneNumber'   => array(AttributeType::String, 'required' => true),
			'gstPercent'   => array(AttributeType::String, 'required' => true),
		);
	}

	public function getSettingsHtml()
	{
		return craft()->templates->render('paypaloots/_settings', array(
			'settings' => $this->getSettings()
		));
	}

	public function addTwigExtension()  
	{
    	Craft::import('plugins.paypaloots.twigExtensions.ArrayCastTwigExtension');

    	return new ArrayCastTwigExtension();
	}

	public function hasCpSection()
    {
        return true;
    }

    	
	public function registerRoutes()
	{
  		return array(
  			'paypaloots/listing' => array('action' => 'paypaloots/listing'),
  			'paypaloots/email' => array('action' => 'paypaloots/sendEmail'),
  			'paypaloots/index' => array('action' => 'paypaloots'),
  			'paypaloots/export' => array('action' => 'paypaloots/export'),
  			'paypaloots/new' => array('action' => 'paypaloots/new'),
    		'paypaloots/add' => array('action' => 'paypaloots/add'),
    		'paypaloots/payment' => array('action' => 'paypaloots/payment'),
    		'paypaloots/confirm' => array('action' => 'paypaloots/confirm'),
    		'paypaloots/cancel' => array('action' => 'paypaloots/cancel'),
    		'paypaloots/invoice' => array('action' => 'paypaloots/invoice'),
    		'paypaloots/list' => array('action' => 'paypaloots/listInvoice'),
    		'paypaloots/invoiceupdate' => array('action' => 'paypaloots/updateInvoice'),
    		'paypaloots/edit/([A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9]-[A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9]-[A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9]-[A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9]-[A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9])' => array('action' => 'paypaloots/editInvoice'), //edit route
  		);
	}
	
    /**
     * Register control panel routes
     */
    
    public function registerCpRoutes()
	{
  		return array(
  			'paypaloots/listing' => array('action' => 'paypaloots/listing'),
  			'paypaloots/email' => array('action' => 'paypaloots/sendEmail'),
  			'paypaloots/index' => array('action' => 'paypaloots'),
  			'paypaloots/export' => array('action' => 'paypaloots/export'),
  			'paypaloots/new' => array('action' => 'paypaloots/new'),
    		'paypaloots/add' => array('action' => 'paypaloots/add'),
    		'paypaloots/payment' => array('action' => 'paypaloots/payment'),
    		'paypaloots/confirm' => array('action' => 'paypaloots/confirm'),
    		'paypaloots/cancel' => array('action' => 'paypaloots/cancel'),
    		'paypaloots/invoice' => array('action' => 'paypaloots/invoice'),
    		'paypaloots/list' => array('action' => 'paypaloots/listInvoice'),
    		'paypaloots/invoiceupdate' => array('action' => 'paypaloots/updateInvoice'),
    		'paypaloots/edit/([A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9]-[A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9]-[A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9]-[A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9]-[A-Z0-9][A-Z0-9][A-Z0-9][A-Z0-9])' => array('action' => 'paypaloots/editInvoice'), //edit route
  		);
	}
}