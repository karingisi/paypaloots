<?php
namespace Craft;

/**
 * Paypaloots service
 */
//include('/Applications/MAMP/htdocs/TSM-The-Snowy-Mountains/extensions/plugins/Paypaloots/bootstrap.php');
require __DIR__ . '/../bootstrap.php';

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\Tax;

//other includes
use PayPal\Api\Address;
use PayPal\Api\CreditCard;
use PayPal\Api\FundingInstrument;

//paypal authorization
use PayPal\Rest\ApiContext as ApiContext;
use PayPal\Auth\OAuthTokenCredential;

//execute payment
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;

//recurring payments

//billing plan
use PayPal\Api\Plan;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\Currency;
use PayPal\Api\ChargeModel;

//billing agreement
use PayPal\Api\Agreement;
use PayPal\Api\ShippingAddress;

//invoice
use PayPal\Api\Invoice;
use PayPal\Api\MerchantInfo;
use PayPal\Api\BillingInfo;
use PayPal\Api\InvoiceItem;
use PayPal\Api\Phone;
use PayPal\Api\PaymentTerm;
use PayPal\Api\InvoiceAddress;

//invoice search
use PayPal\Api\Search;


class PaypalootsService extends BaseApplicationComponent
{
     protected $paypalootsRecord;

    /**
     * Create a new instance of the Paypaloots Service.
     * Constructor allows PaypalootsRecord dependency to be injected to assist with unit testing.
     *
     * @param @paypalootsRecord to access the database
     */
    public function __construct($paypalootsRecord= null)
    {
        $this->paypalootsRecord = $paypalootsRecord;
        if (is_null($this->paypalootsRecord)) {

            $this->paypalootsRecord = PaypalootsRecord::model();
        }
    }

    //paypal functions
    public function createPaypalPayment(){
        // ### Payer
        // A resource representing a Payer that funds a payment
        // For paypal account payments, set payment method
        // to 'paypal'.
        
        $settings = craft()->plugins->getPlugin('paypaloots')->getSettings();  //get settings here
        $chamberDiscount = 0;
        $marketingAmount = 0;
        //$apiContext = getApiContext(); //call getApiContext, check for errors heres

        try {
            $apiContext = getApiContext();

        } catch (PayPal\Exception\PPConnectionException $ex) {
             craft()->userSession->setFlash('api', "Sorry there is a problem connecting to PayPal. Please contact Neil at tsm@snowymountains.com.au!");
            exit(1);
        }

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $member = craft()->httpSession->get('member'); //get session member data
        
        /*if($member['regionalChamber'] == 'Yes'){
            $chamberDiscount = 0-(($settings->membershipFee) * ($settings->chamberFeeDiscount) * 0.01); //15% default
        }*/

        if($member['marketingCampaign'] == 'Yes'){
            $marketingAmount = floatval($member['marketingAmount']) + floatval($member['marketingAmount'])*(($settings->gstPercent)* 0.01); //add 18% VAT
        }
        
        // ### Itemized information
        $item1 = new Item();

        $item1->setName('TSM Membership (Includes 10% GST)')
            ->setDescription('TSM Membership Fee')
            ->setCurrency('AUD')
            ->setQuantity(1)
            ->setPrice($settings->membershipFee);

        $item2 = new Item();

        /*$item2->setName('Regional Chamber Discount')
            ->setDescription('Chamber Discount')
            ->setCurrency('AUD')
            ->setQuantity(1)  //quantity is single membership
            ->setPrice($chamberDiscount); */  

        $item3 = new Item();

        $item3->setName('TSM Marketing Contribution')
            ->setDescription('Contribution + GST')
            ->setCurrency('AUD')
            ->setQuantity(1)  //quantity is single membership
            ->setPrice($marketingAmount);        

        $itemsArray = [$item1];  //array for items
        $totalAmounts = $item1->getPrice();  //array for amounts

        /*if($member['regionalChamber'] == 'Yes'){
            array_push($itemsArray,$item2);
            $totalAmounts = $totalAmounts + $item2->getPrice(); 
        }*/

        if($member['marketingCampaign'] == 'Yes'){
            array_push($itemsArray,$item3);
           $totalAmounts = $totalAmounts + $item3->getPrice(); 
        }

        $itemList = new ItemList();
        //$itemList->setItems(array($item1, $item2));
        $itemList->setItems($itemsArray);
        // ### Additional payment details
        // Use this optional field to set additional
        // payment information such as tax, shipping
        // charges etc.
        $details = new Details();
        $details->setShipping('0.00')
            ->setTax('0.00')
            ->setSubtotal($totalAmounts);

        // ### Amount
        // Lets you specify a payment amount.
        // You can also specify additional details
        // such as shipping, tax.
        $amount = new Amount();
        $amount->setCurrency("AUD")
            ->setTotal($totalAmounts)
            ->setDetails($details);

        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. 
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
             ->setDescription("Payment description");

        // ### Redirect urls
        // Set the urls that the buyer must be redirected to after 
        // payment approval/ cancellation.
        $baseUrl = getBaseUrl();

        $redirectUrls = new RedirectUrls();

        $redirectUrls->setReturnUrl("$baseUrl/index.php/paypaloots/confirm")
            ->setCancelUrl("$baseUrl/cancel");

        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent set to 'sale'
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        // ### Create Payment
        // Create a payment by calling the 'create' method
        // passing it a valid apiContext.
        // (See bootstrap.php for more on `ApiContext`)
        // The return object contains the state and the
        // url to which the buyer must be redirected to
        // for payment approval
        try {
            $payment->create($apiContext);

        } catch (PayPal\Exception\PPConnectionException $ex) {
            //echo "Exception: " . $ex->getMessage() . PHP_EOL;
             craft()->userSession->setFlash('api', "Sorry there is a problem connecting to PayPal. Please contact Neil at tsm@snowymountains.com.au!");
            exit(1);
        }

        // ### Get redirect url
        // The API response provides the url that you must redirect
        // the buyer to. Retrieve the url from the $payment->getLinks()
        // method
        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                 $redirectUrl = $link->getHref();
                 break;
            }
        }
        // ### Redirect buyer to PayPal website
        // Save the payment id so that you can 'complete' the payment
        // once the buyer approves the payment and is redirected
        // back to your website.
        //
        // It is not a great idea to store the payment id
        // in the session. In a real world app, you may want to 
        // store the payment id in a database.
        //$_SESSION['paymentId'] = $payment->getId();

        craft()->httpSession->add('paymentId', $payment->getId());

        if(isset($redirectUrl)) {
            header("Location: $redirectUrl"); //redirect using header
            exit; 
        }
    }

    //execute service
    public function execute(){

        // Get the payment Object by passing paymentId
        // payment id was previously stored in session in
        $apiContext = getApiContext(); //call getApiContext

        //$paymentId = $_SESSION['paymentId'];
        $paymentId  = craft()->httpSession->get('paymentId');

        $payerId = craft()->request->getParam('PayerID');  //get PayerID
        
        $payment = Payment::get($paymentId, $apiContext); //$apiContext   //get the paymentId from session
        // PaymentExecution object includes information necessary 
        // to execute a PayPal account payment. 
        // The payer_id is added to the request query parameters
        // when the user is redirected from paypal back to your site
        $execution = new PaymentExecution();   //execute payment here
        //$payerId = $variables['PayerID'];
        $execution->setPayerId($payerId); //payerID
        //execute the payment here
        $result = $payment->execute($execution, $apiContext);

        //converting payment object to array
        $variables['confirm'] = (array)$result;  //is type casting array here the best option?
        
        return $variables;   
    }


    //function to create member listing in the system after user has registered.
    //need to check if listing exists, if yes don't do anything ELSE create it
    //create structure entry.
    //it will create a draft...Admin will need to enable the listing manually.
    public function createMemberListing(){

        //start with accommodation (EntryType: id=3, sectionId=3, name=Accommodation, handle=accommodation) 
        $title = "Five";
        $heading = "Five";
        $body = "Testing entry";
        $phone = "0411302326";
        $email = "silvanus@outofthesquare.com.au";
        $websiteAddress = "http://www.outofthesquare.com.au";
        $towns = "Tumbarumba";

        $listingType = 63; // fieldID , "Standard Listing";
        
        $street1 = "27 Watt Street";
        $city = "Newcastle";
        $state = "NSW";
        $zip = "2300";


        $criteria = craft()->elements->getCriteria(ElementType::Category);
        $criteria->title = array('Accommodation','Activity','Attraction');
        $accommodation = craft()->elements->findElements($criteria);
        $accommodationArray = array();

        foreach($accommodation as $acc){
            array_push($accommodationArray,$acc->id);
        }

        var_dump($accommodationArray);

        //coming from the form
        /*$title = $member['businessName'];
        $heading = $member['businessName'];
        $body = $member['body'];

        $address = $member['street1']; 
        $phone = $member['phone'];
        $email = $member['email'];
        $websiteAddress = $member['websiteAddress'];
        $towns = $member['city'];
        $listingType = "Standard Listing";  
        $accommodationType = "Hotels";
        $businessCategory = $member['businessCategory'];*/

        /*$criteria = craft()->elements->getCriteria(ElementType::Category);
        $criteria->title = array('TravelAgent','Hotels');
        $accommodationTypeCategory = craft()->elements->findElements($criteria);
        $accommodationTypeArray = array();

        foreach($accommodationTypeCategory as $cat){
            array_push($accommodationTypeArray,$cat->id);
        }

        //create entry
        $entry = new EntryModel();
        
        $entry->sectionId = 3;
        $entry->typeId = 3;
        $entry->authorId = 1;
        //$entry->postDate = date('Y-m-d h:i:s');
        //$entry->expiryDate = date('Y-m-d h:i:s');
        $entry->enabled = true;

        $entry->setContent(
            ['title' => $title, 'body' => $body, 
             'address'=>['street1'=>'27 Watt Street', 'street2'=>'' ,'city'=>'Newcastle', 'state'=>'NSW',
                                        'zip'=>2300, 'country'=>'Australia', 'lat'=>'', 'lng'=>''],

             'phone'=>$phone,'websiteAddress'=>$websiteAddress,
             'towns'=>$towns, 'accommodationType'=>$accommodationTypeArray, 'listingType'=>$listingType ]);

        try{

            if(craft()->entries->saveEntry($entry)){
               echo "saved!"; 
            }

        }catch(\Exception $ex){
            //var_dump($ex);
            echo "Error: Unable to save listing!";
        }*/
    }

    //output listing, etc 
    public function searchListing(){

    }

    //create a Plan - Recurring Payment
    public function createPlan()
    {
        // Create a new instance of Plan object
        $plan = new Plan();

        // Fill up the basic information that is required for the plan
        $plan->setName('Tourism Snowy Mountains')
            ->setDescription('Membership Fee')
            ->setType('fixed');

        // # Payment definitions for this billing plan.
        $paymentDefinition = new PaymentDefinition();

        $settings = craft()->plugins->getPlugin('paypaloots')->getSettings(); //settings from plugin

        $member = craft()->httpSession->get('member'); //get session member data
        
        if($member['regionalChamber'] == 'Yes'){

            $membershipFee = $settings->membershipFee - (($settings->membershipFee) * ($settings->chamberFeeDiscount) * 0.01); //15% default

        }else{

            $membershipFee = $settings->membershipFee;
        }

        // The possible values for such setters are mentioned in the setter method documentation.
        // Just open the class file. e.g. lib/PayPal/Api/PaymentDefinition.php and look for setFrequency method.
        // You should be able to see the acceptable values in the comments.
        $paymentDefinition->setName('Regular Payments')
            ->setType('REGULAR')
            ->setFrequency('YEAR')
            ->setFrequencyInterval("1")
            ->setCycles("1")
            ->setAmount(new Currency(array('value' => $membershipFee, 'currency' => 'AUD')));

        // Charge Models
        $chargeModel = new ChargeModel();
        $chargeModel->setType('SHIPPING')
            ->setAmount(new Currency(array('value' => 0, 'currency' => 'AUD')));

        $paymentDefinition->setChargeModels(array($chargeModel));

        $merchantPreferences = new MerchantPreferences();
        $baseUrl = getBaseUrl();
        // ReturnURL and CancelURL are not required and used when creating billing agreement with payment_method as "credit_card".
        // However, it is generally a good idea to set these values, in case you plan to create billing agreements which accepts "paypal" as payment_method.
        // This will keep your plan compatible with both the possible scenarios on how it is being used in agreement.
        $merchantPreferences->setReturnUrl("$baseUrl/ExecuteAgreement.php?success=true")
            ->setCancelUrl("$baseUrl/ExecuteAgreement.php?success=false")
            ->setAutoBillAmount("yes")
            ->setInitialFailAmountAction("CONTINUE")
            ->setMaxFailAttempts("0")
            ->setSetupFee(new Currency(array('value' => 0, 'currency' => 'AUD')));

        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);

        // For Sample Purposes Only.
        $request = clone $plan;

        // ### Create Plan
        try {

            $output = $plan->create($apiContext); //created plan

        } catch (Exception $ex) {
            //ResultPrinter::printError("Created Plan", "Plan", null, $request, $ex);
            craft()->userSession->setFlash('recurring', "Sorry there is a problem in creating recurring payment plan. Please contact Neil at tsm@snowymountains.com.au!");
            //exit(1);
        }

        //ResultPrinter::printResult("Created Plan", "Plan", $output->getId(), $request, $output);
        return $output;
    }

    //Activate a Plan

    //Create an Agreement
    public function createAgreement()
    {

        $agreement = new Agreement();
        $startDate = date('Y-m-dTg:i:sZ'); //starting today

        $agreement->setName('TSM Membership Renewal Agreement')
            ->setDescription('Basic Agreement')
            ->setStartDate($startDate);

        // Add Plan ID
        // Please note that the plan Id should be only set in this case.
        $createdPlan = $this->createPlan(); //return created plan here
            
        $plan = new Plan();
        $plan->setId($createdPlan->getId());
        $agreement->setPlan($plan);

        // Add Payer
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);

        // For Sample Purposes Only.
        $request = clone $agreement;

        // ### Create Agreement
        try {
            // Please note that as the agreement has not yet activated, we wont be receiving the ID just yet.
            $agreement = $agreement->create($apiContext);
            // ### Get redirect url
            // The API response provides the url that you must redirect
            // the buyer to. Retrieve the url from the $agreement->getApprovalLink()
            // method
            $approvalUrl = $agreement->getApprovalLink();

        } catch (Exception $ex) {
             craft()->userSession->setFlash('billing', "Sorry there is a problem in creating Membership billing agreement. Please contact Neil at tsm@snowymountains.com.au!");
            //exit(1);
        }
        //echo "Created Billing Agreement. Please visit the URL to Approve.", "Agreement", "<a href='$approvalUrl' >$approvalUrl</a>";
        return $agreement;
    }


    //Redirect user to Paypal using the approval link

    //Paypal redirects user back to my site

    //Execute agreement
    public function executeAgreement(){

        if (isset($_GET['success']) && $_GET['success'] == 'true') {

            $token = $_GET['token'];
            $agreement = new \PayPal\Api\Agreement();
            try {
                // ## Execute Agreement
                // Execute the agreement by passing in the token
                $agreement->execute($token, $apiContext);

            } catch (Exception $ex) {
                craft()->userSession->setFlash('executeagreement', "Sorry there is a problem in executing agreement. Please contact Neil at tsm@snowymountains.com.au!");
                //exit(1);
            }

            // Make a get call to retrieve the executed agreement details
            try {
                $agreement = \PayPal\Api\Agreement::get($agreement->getId(), $apiContext);

            } catch (Exception $ex) {

                 craft()->userSession->setFlash('erroragreement', "Sorry there is a error in executing agreement. Please contact Neil at tsm@snowymountains.com.au!");
                //exit(1);
            }
            //echo "Agreement has been approved!";

        } else {
            //echo "User cancelled the approval";
            //redirect user to the website
        }
    }

    //end paypal functions
    public function add(PaypalootsModel &$model)
    {
        $attributes = array(
       						'type' =>$model->type,
       						'amount'=>$model->amount, 
       						'currency'=>$model->currency, 
       						'funding_source'=>$model->funding_source, 
                            'payment_resource'=>$model->payment_resource,
                            'transaction_id'=>$model->transaction_id, 
       						'status'=>$model->status
       					   );
       
       $record->setAttributes($attributes,false);  //if you don't put false it won't save for non-mandatory field.
       $record->save();
    }

    //create invoice
    //return created invoice
    public function createInvoice($billingEmail){

        $settings = craft()->plugins->getPlugin('paypaloots')->getSettings();  //get settings here

        $invoice = new Invoice();
        // ### Invoice Info
        // Fill in all the information that is
        // required for invoice APIs
        $invoice
            ->setMerchantInfo(new MerchantInfo())
            ->setBillingInfo(array(new BillingInfo()))
            ->setNote("Membership Renewal")
            ->setPaymentTerm(new PaymentTerm());

        // ### Merchant Info
        // A resource representing merchant information that can be
        // used to identify merchant
        $invoice->getMerchantInfo()
            ->setEmail($settings->paypalEmail)
            ->setFirstName("Tourism")
            ->setLastName("Snowy")
            ->setbusinessName("Snowy Mountains")
            ->setPhone(new Phone())
            ->setAddress(new Address());

        $invoice->getMerchantInfo()->getPhone()
            ->setCountryCode("61")
            ->setNationalNumber($settings->phoneNumber);

        // ### Address Information
        // The address used for creating the invoice
        $invoice->getMerchantInfo()->getAddress()
            ->setLine1($setting->streetAddress)
            ->setCity($settings->city)
            ->setState("NSW")
            ->setPostalCode($setting->postcode)
            ->setCountryCode("AU");

        // ### Billing Information
        // Set the email address for each billing
        $billing = $invoice->getBillingInfo();
        $billing[0]
            ->setEmail($billingEmail);

        // ### Items List
        // You could provide the list of all items for
        // detailed breakdown of invoice
        $items = array();
        $items[0] = new InvoiceItem();
        $items[0]
            ->setName("Membership Fee")
            ->setQuantity(1)
            ->setUnitPrice(new Currency());

        $items[0]->getUnitPrice()
            ->setCurrency("AUD")
            ->setValue(1);

        // #### Tax Item
        // You could provide Tax information to each item.
        $tax = new \PayPal\Api\Tax();
        $tax->setPercent(1)->setName("Tax");
        $items[0]->setTax($tax);

        $invoice->setItems($items);

        $invoice->getPaymentTerm()
            ->setTermType("NET_45");

        // ### Logo
        // You can set the logo in the invoice by providing the external URL pointing to a logo
        $invoice->setLogoUrl('https://www.paypalobjects.com/webstatic/i/logo/rebrand/ppcom.svg');

        // For Sample Purposes Only.
        $request = clone $invoice;

        try {
            // ### Create Invoice
            // Create an invoice by calling the invoice->create() method
            // with a valid ApiContext (See bootstrap.php for more on `ApiContext`)
            $apiContext = getApiContext();

            $invoice->create($apiContext);

        } catch (Exception $ex) {
            craft()->userSession->setFlash('api', "There is error creating invoice. Please contact Neil at tsm@snowymountains.com.au!");
            exit(1);
        }
        //send invoice
        $this->sendInvoice($invoice,$apiContext);
    }

    //send invoice
    public function sendInvoice($invoice,$apiContext){

        try {
        // ### Send Invoice
        // Send a legitimate invoice to the payer
        // with a valid ApiContext (See bootstrap.php for more on `ApiContext`)
        $sendStatus = $invoice->send($apiContext);

        } catch (Exception $ex) {
            craft()->userSession->setFlash('api', "There is error sending invoice. Please contact Neil at tsm@snowymountains.com.au!");
            exit(1);
        }
       // echo "invoice sent!";
    }

    //update invoice number to whatever number she wants from MYOB
    public function updateInvoice($invoiceId,$invoiceNumber){ 

        try {
            $apiContext = getApiContext();
            
            $invoice = Invoice::get($invoiceId,$apiContext);
            $invoice->setNumber($invoiceNumber);
            //need to set Metadata for some reason, otherwise wont work
            $invoice->setMetadata( array('created_date' => date('Y-d-j') ));
            $invoice->setTaxInclusive(0);

            //set tax on invoice item
            $items = $invoice->getItems();
            $items[0]->setTax(0);

            $invoice->update($apiContext);

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            //echo "Error: unable to update invoice";
            echo $ex->getData();
            exit(1);
        }
       // echo "invoice updated successfully";
    }

    //getInvoiceById
    public function getInvoiceById($invoiceId){ 

        try {
            $apiContext = getApiContext();
            
            $invoice = Invoice::get($invoiceId,$apiContext);
            return $invoice;

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            //echo "Error: unable to update invoice";
            echo $ex->getData();
            exit(1);
        }
        //echo "<br>";
       //echo "invoice updated successfully";
    }


        //return all invoices
    public function listInvoice(){

        try {
            $apiContext = getApiContext();

            $invoices = Invoice::getAll(array('page' => 0,'total_count_required' => "false"), $apiContext);

            return $invoices->invoices;
        } catch (Exception $ex) {
            craft()->userSession->setFlash('api', "Error,unable to list invoices");
            exit(1);
        }
        //echo "Lookup Invoice History";
    }

    //search invoice with status='PAID'
    public function paidInvoices(){

        try {
            $apiContext = getApiContext();
            // ### Search Object
            // Fill up your search criteria for Invoice search.
            // Using the new way to inject raw json string to constructor
            $search = new Search(
                '{
                  "email": "silvanus.matiku@gmail.com",
                  "page" : 1,
                  "page_size" : 20,
                  "total_count_required" : true
                }'
            );

            $search->setStatus("sent");
            // ### Search Invoices
            // Retrieve the Invoice History object by calling the
            // static `search` method on the Invoice class.
            // Refer the method doc for valid values for keys
            // (See bootstrap.php for more on `ApiContext`)
            $invoices = Invoice::search($search, $apiContext);
            
        } catch (Exception $ex) {
             var_dump($ex->getData()); //error, flush error
            //echo "Can not search paid invoices";
             exit(1);
        }
       //flash success here         
    }

    //finish send invoice
    //end create invoice
    //getAll records
    public function getAllTransactions()
    {
        $records = $this->paypalootsRecord->findAll(array('order'=>'t.dateCreated'));
        return PaypalootsModel::populateModels($records, 'id'); //populate models into models array by id ascending, it will return array of all models
    }

    //get total transactions
     public function getTotalTransactions()
    {
        $records = $this->paypalootsRecord->findAll(array('order'=>'t.dateCreated'));
        return count($records);
    }

    //get transaction by Id
      public function getTransactionById($id)
    {
        $record = $this->paypalootsRecord->findById($id); //search record by id from the paypaloots_records table
        return $record;
    }

    //send email
    public function sendEmailTo($toEmail,$subject,$body){

        $paypalootsSettings = craft()->plugins->getPlugin('paypaloots')->getSettings();

        $email            = new EmailModel();
        $emailSettings    = craft()->email->getSettings();

        $email->fromEmail = $emailSettings['emailAddress'];
        $email->replyTo   = $emailSettings['emailAddress'];
        $email->sender    = $emailSettings['emailAddress'];
        $email->fromName  = "Tourism Snowy Mountains";
        $email->toEmail   = $toEmail;
        //$email->cc        = $paypalootsSettings->tsmEmailcc;
        $email->subject   = $subject;
        $email->body      = $body;

        craft()->email->sendEmail($email);
    }
   
	/**
	 * Fires an 'onBeforeSend' event.
	 *
	 * @param PaypalootsEvent $event
	 */
	public function onBeforeSend(PaypalootsEvent $event)
	{
		$this->raiseEvent('onBeforeSend', $event);
	}
}